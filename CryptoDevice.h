#pragma once

#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <cstdio>
#include <iostream>
#include <string.h>
#include <cstdlib>
#include <md5.h>
#include <hex.h>
#include "osrng.h"
#include "modes.h"

#define MAX_NUM_OF_TRIES 3

class CryptoDevice
{

public:
	CryptoDevice();
    std::string encryptAES(std::string);
    std::string decryptAES(std::string);
    


private:
	std::string hash_md5(std::string);
	bool login();
	CryptoPP::byte key[CryptoPP::AES::DEFAULT_KEYLENGTH], iv[CryptoPP::AES::BLOCKSIZE];
	std::string hexDigest_pass = "165902d3237e71816fd67991d72669f8"; // password hash
	std::string hexDigest_user = "7e989d45552410d16579f6821b8dbe27"; // user_name hash
};
