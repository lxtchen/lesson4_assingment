import hashlib
import itertools
import cmd
import argparse
import time

def parseArgs():
	"""
		this function parses the arguments from the command line
	"""
	parser = argparse.ArgumentParser() # creating the parser
	
	# MD5 hash
	parser.add_argument("md5", help="the known md5 hash of the password") 
	# Chars Range
	parser.add_argument("charsrange", choices=["a", "aA", "a1", "A", "A1", "1", "aA1"], help="the characters included in the password")
	# Length
	parser.add_argument("length", help="the length of the password", type=int)
	# --Salt
	parser.add_argument("-s", "--salt", help="a salt for the md5 hash")
	
	args = parser.parse_args() # saving the arguments into args
	
	return args

def getArgs():
	"""
		store the arguments from the command line and returning the option choosen
	"""
	
	# arguments: args. -> md5 / charsrange / length / salt
	args = parseArgs()
	
	return args

	
def mdbruce(args):
	"""
		brute force on a known md5
	"""
	
	# char set, setting
	charset = ""
	if 'a' in args.charsrange: # lower case abc
		charset = charset + "abcdefghijklmnopqrstuvwxyz"
	if 'A' in args.charsrange: # upper case ABC
		charset = charset + "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	if '1' in args.charsrange: # numbers
		charset = charset + "0123456789"
		
	# salt
	salt = b""
	if args.salt: # there is salt
		salt = (args.salt).encode('ascii')
		
		
	print ("Bruce-Force running...\n")
	password = ""
	start_time = time.time()
	
	# running the brute-force	
	for guess in itertools.product(charset, repeat=args.length):
		guess = ''.join(guess) # joins the chars in the guess into a string
		hash = (hashlib.md5(salt + guess.encode())).hexdigest() # converts the string with the salt(if included) into a digset
		if hash == args.md5:
			password = guess
			break
	
	print (time.time()-start_time,"seconds") # the time it took to complete the bruteforce
	
	if password: # the password was cracked
		print ("done! the password is: " , password)
	else:
		print ("coudn't crack md5.")



def main():
	
	mdbruce(getArgs())
	
main()
