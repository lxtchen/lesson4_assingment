#include "CryptoDevice.h"
#include <windows.h>
//aquire a default key for AES and block size for CBC. can't generate randomly, since all the agents shuold have the same key.
//can initialize with a unique sequence but not needed.

CryptoDevice::CryptoDevice()
{ 
	//BURN KEY TO THE DEVICE
	// the key of the encryption and decryption method
	key[0] = 0;
	key[1] = 0;
	key[2] = 7;
	key[3] = 0;
	key[4] = 0;
	key[5] = 7;
	key[6] = 0;
	key[7] = 0;
	key[8] = 7;
	key[9] = 0;
	key[10] = 0;
	key[11] = 7;
	key[12] = 0;
	key[13] = 0;
	key[14] = 7;
	key[15] = 7;
	//

	// LOGIN
	std::cout << "please insert details:" << std::endl;
	try 
	{
		while (!login()) {} // login tries
	}
	catch (const char* s)
	{
		std::cout << s << std::endl;
		Sleep(1743); // for the dramatic reaction lol
		exit(-1); // "power-off the device"
	}

	system("cls"); // clearing start up output
	std::cout << "ACCESS GRANTED! welcome back agent 7" << std::endl << std::endl;
}

bool CryptoDevice::login()
{
	static int counter = 0; // will cound the number of tries the user did
	std::string input;
	std::string hashed_user_name;
	std::string hashed_password;

	/// tries
	if (counter == MAX_NUM_OF_TRIES) { throw "ACCESS DENIED! starting driver erase..."; }
	else if (counter > 0) { std::cout << "ACCESS DENIED! " << MAX_NUM_OF_TRIES - counter << " tries left." << std::endl; }
	///

	std::getline(std::cin, input); // getting the input from the user

	/// spliting the input to user_name and password
	size_t position = input.find(" ") + 1; // the postion of the password
	std::string user_name = input.substr(0, position-1);
	std::string password = input.substr(position);
	///

	// hashing the input
	hashed_user_name = hash_md5(user_name); 
	hashed_password = hash_md5(password);

	// checking the user name and password that was entered. correct - returns true / incorrect - inc(counter) and return false / 3 tries used - throw a driver erasing command (anti-data-theft)
 	return (hashed_user_name == hexDigest_user) ? ((hashed_password == hexDigest_pass) ? true : !(++counter)) : !(++counter);
}

std::string CryptoDevice::hash_md5(std::string message)
{
	std::string salt = "bond"; // the salt of the company

	CryptoPP::byte digest[CryptoPP::Weak::MD5::DIGESTSIZE]; // holds the hash in bytes

	CryptoPP::Weak::MD5 hash;
	hash.CalculateDigest(digest, (const CryptoPP::byte*)(salt + message).c_str(), message.length() + salt.length()); // calculating the hash and putting it into the digest

	CryptoPP::HexEncoder encoder;
	std::string hashed_message;

	/// transfering the hash as hexString into a regular string
	encoder.Attach(new CryptoPP::StringSink(hashed_message));
	encoder.Put(digest, sizeof(digest));
	encoder.MessageEnd();
	///

	std::transform(hashed_message.begin(), hashed_message.end(), hashed_message.begin(), ::tolower); // lower casing every char in the string

	/// md5 is known for its security problams and vulnerabilitys
	/// i have a tool that can crack an md5 hash into a password
	/// i added it to the repository for you to check it out.
	return hashed_message;
}

std::string CryptoDevice::encryptAES(std::string plainText)
{
    std::string cipherText;
	
	CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, iv);

	CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink(cipherText));
	  //StringSink created with new does not require explicit destruction, but this is only because THIS library is build this way
	stfEncryptor.Put(reinterpret_cast<const CryptoPP::byte*>(plainText.c_str()), plainText.length() + 1);
	stfEncryptor.MessageEnd();

	return cipherText;
}

std::string CryptoDevice::decryptAES(std::string cipherText)
{

    std::string decryptedText;
	
	CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);

	CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(decryptedText));
	  //StringSink created with new does not require explicit destruction, but this is only because THIS library is build this way
	stfDecryptor.Put(reinterpret_cast<const CryptoPP::byte*>(cipherText.c_str()), cipherText.size());
	stfDecryptor.MessageEnd();

	return decryptedText;
}