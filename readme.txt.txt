version 1.007:
	no comment

version 2.007:
	i added a crack tool that i built for you to try and crack the password for the main agent.
	once in an operation i saw a part of his password. i think it starts with "sk".
	the md5 algorithem used in the CryptoDevice uses salt, i digged the source files of it and
	got this "bond". you can try and connect the two and run the tool.
	hash = generate(salt+password)
	hash = generate("bondsk"+other_part_of_the_password)
	this is the hash of the password good luck: "165902d3237e71816fd67991d72669f8" (there is no need for the "" when using the tool)

future update version 3.007 concept:

	client_start
	||
	\/
	generating public and private keys
	||
	\/
	sends the public key to the operator which forwards it
	to all the other agents
	||
	\/
	now, if someone want to send a private message to that
	agent, he would need to encrypt it with the public key sent to him.
	then send it to the operator that will forward it to the agent
	||
	\/
	when the agent get the message only him can decrypt it with his
	private key (you can identify rsa encrypted messages from regular aes)
	||
	\/
	protocol done!